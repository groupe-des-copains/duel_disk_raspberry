const tesseract = require("node-tesseract-ocr")

const config = {
  lang: "eng", // default
  oem: 3,
  psm: 6,
}

async function main() {
  console.log("Reconnaissance ....")
  try {
    const img = "data/code.jpg";
        const text = await tesseract.recognize(img, config);
        console.log("code de la carte : ", text);
  } catch (error) {
    console.log(error.message);
  }
}

main()