const Gpio = require('onoff').Gpio; // utilisation du module Gpio
const io = require('socket.io-client');
const socket = io("ws://192.168.1.178:3000/", {
    reconnectionDelayMax: 10000,
    auth: {
        token: "123"
    },
    query: {
        value: "my-value"
    }
});

const code = "MOCK_CODE";

const sep = () => {
    console.log("----------------------------------------------");
}

class Slot {
    constructor(type, pin, number) {
        this.type = type;
        this.pin = pin;
        this.number = number;
        this.pushButton = 0;
        this.pushButton = new Gpio(this.pin, "in", "both");
        this.isUsed = false;
    }

    watch() {
        // sep();
        console.log("watch btn", this.type, this.number);
        this.pushButton.watch((err, value) => {
            if (!value) {
                console.log("add")
                socket.emit('card_add', { type: this.type, number: this.number, code: code });
                this.isUsed = true;
            }
            else if(this.isUsed === true){
                console.log("delete")
                socket.emit('card_delete', { type: this.type, number: this.number });
            }
        });
    }

    unwatch() {
        sep();
        console.log("unwatch btn", this.type, this.number);
        this.pushButton.unwatch((err, value) => {
            console.log("err", err);
            console.log("value", value);
        });;
    }
}

class MonsterSlot extends Slot {
    constructor(pin, number) {
        super("monster", pin, number);
    }
}

class LinkSlot extends Slot {
    constructor(pin, number) {
        super("link", pin, number);
    }
}

class MagicTrapSlot extends Slot {
    constructor(pin, number) {
        super("magictrap", pin, number);
    }
}

sep();
console.log("Dueldisk started");
sep();
const testSlot = new MonsterSlot(4, 0);
testSlot.watch();
// { device: 'Raspberry 3b+', name: "001" }
socket.emit('connection', { device: 'Raspberry 3b+', name: "001" });


